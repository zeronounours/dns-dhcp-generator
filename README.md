DNS DHCP Generator
==================

Script Ruby pour générer automatiquement la configuration DNS et DHCP
sur un réseau, à partir d'un liste YAML.                             
                                                                     
Le script charge le fichier YAML `CONF_FILE`, devant avoir le format   
spécifié dans le fichier d'example. Il génère ensuite les fichiers de
configuration listés dans `GENERATE` en utilisant les templates ERB    
associés.                                                            


Usage
-----
Une fois [configuré](#configuration), il suffit de lancer le script :
```bash
ruby dns_dhcp_gen.rb
```


Configuration
-------------
Le script est configurable en deux endroits. Le premier se trouve dans la
section de configuration du script lui-même.
Il suffit d'adapter les deux constantes `CONF_FILE` et `GENERATE`.

* `CONF_FILE` est le chemin du fichier YAML contenant la liste de tous les
hôtes à créer sur le réseau. Ce nom est relatif au dossier contenant le script.
* `GENERATE` est un tableau dont chaque élément spécifie un fichier à générer.
Chaque élément est lui-même un tableau d'exactement 2 composants. Le premier
est le chemin vers le fichier de template à utiliser, le second est le chemin
vers le fivhier à créer.

Pour ensuite adapter la configuration de chaque fichier générer, il faut
modifier directement le fichier de template défini plus haut.
Pour plus d'information sur les fichiers de template, voir plus loin la section
[template](#template).


Template
--------
Chaque fichier de template doit être un fichier suivant les spécifications de
format de template ERB.

Pour pouvoir générer dynamiquement la liste ds hôtes, il est possible de faire
appel à la méthode `hosts` qui renvoie la liste de tous les hôtes définis dans
le fichier de configuration.

Pour chaque hôte, il est possible de récupérer les informations suivantes :
* Le nom d'hôte : `host.name`
* L'adresse IP associée : `host.ip`
* L'adresse MAC : `hosts.mac`
* La liste des aliases : `hosts.aliases`

De plus pour les fichiers de configuration nécessitant un identifiant unique,
comme le __serial__ des DNS, l'appel de la méthode `serial` génèrera un cet
identifiant. A noter que celui croira avec le temps, restant cohérent avec les
nécéssités du __serial__ des configurations DNS.
