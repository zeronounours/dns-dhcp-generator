#!/usr/bin/env ruby
#=======================================================================#
#-----------------------------------------------------------------------#
#                          DNS DHCP Generator                           #
#-----------------------------------------------------------------------#
#***********************************************************************#
#                       zeroNounours - 13/07/2015                       #
#-----------------------------------------------------------------------#
#                                                                       #
# Script Ruby pour générer automatiquement la configuration DNS et DHCP #
# sur un réseau, à partir d'un liste YAML.                              #
#                                                                       #
# Le script charge le fichier YAML CONF_FILE, devant avoir le format    #
# spécifié dans le fichier d'example. Il génère ensuite les fichiers de #
# configuration listés dans GENERATE en utilisant les templates ERB     #
# associés.                                                             #
#                                                                       #
#-----------------------------------------------------------------------#
#                               LICENSE                                 #
# Copyright 2015 Gauthier Sebaux (zeroNounours) <gauthier@sebaux.eu>    #
#                                                                       #
# This file is part of DNS DHCP Generator.                              #
#                                                                       #
# DNS DHCP Generator is free software: you can redistribute it and/or   #
# modify it under the terms of the GNU General Public License as        #
# published by the Free Software Foundation, either version 3 of the    #
# License, or (at your option) any later version.                       #
#                                                                       #
# DNS DHCP Generator is distributed in the hope that it will be useful, #
# but WITHOUT ANY WARRANTY; without even the implied warranty of        #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         #
# GNU General Public License for more details.                          #
#                                                                       #
# You should have received a copy of the GNU General Public License     #
# along with DNS DHCP Generator.                                        #
# If not, see <http://www.gnu.org/licenses/>.                           #
#                                                                       #
#-----------------------------------------------------------------------#
#                            HISTORIQUE                                 #
#   V1.0.0    zeroNounours - 13/07/2015                                 #
#             Création du script                                        #
#=======================================================================#


CONF_FILE = 'hosts.yml'

# [[fichier ERB, fichier de sortie], ...]
GENERATE = [
  ['dhcp.erb', '/etc/dhcp/dhcpd.inc'],
  ['dns.erb', '/etc/bind/zone_files/home.zone'],
  ['r_dns.erb', '/etc/bind/zone_files/1.168.192.in-addr.arpa.zone'],
]


DIR = File.dirname(__FILE__)

require 'erb'
require 'yaml'

class Host
  def initialize name, hash
    @info = hash
    @name = name
  end

  def name
    @name.downcase
  end
  def ip
    @info['ip'].to_s
  end
  def mac
    @info['mac'].upcase
  end
  def aliases
    return @info['aliases'] if @info.has_key?('aliases')
    []
  end
end

class Env
  def initialize hosts
    @hosts ||= []
    @hosts = hosts.select{|v| v.is_a? Host} if hosts.is_a?(Array)
  end

  def get_binding
    binding
  end
  def gen_serial
    t = Time.new
    t.strftime('%y%m%d') + (t.hour*360 + t.min*6 + t.sec/10).to_s
  end

  def hosts
    @hosts
  end
  def serial
    @serial ||= gen_serial
  end
end

# Fonction pour passer un fichier ERB à la moulinette
def create_conf from, to, env
  begin
    file = File.new(DIR + '/' + from)
  rescue
    puts "Impossible d'ouvrir le fichier #{from} :\n" + $!.message
    exit 2
  end

  template = ERB.new(file.read, nil, '-')
  file.close
  begin
    content = template.result(env.get_binding)
  rescue
    puts "Syntaxe ERB du fichier #{from} incorrecte :\n" + $!.message
    exit 3
  end

  begin
    File.open(to, 'w+') do |f|
      f.puts content
    end
  rescue
    puts "Impossible d'écrire le fichier #{to} :\n" + $!.message
    exit 2
  end
end

## Parse de la conf

begin
  conf = YAML::load_file(DIR + '/' + CONF_FILE)
rescue
  puts "Impossible de lire le fichier de configuration #{CONF_FILE} :"
  puts $!.message
  exit 2
end

unless conf.is_a? Hash
  puts "Le premier niveau du fichier de configuration doit être un Hash"
  exit 3
end

tmp = []
conf.each do |n,h|
  tmp << Host.new(n,h)
end

env = Env.new(tmp)

# Création des fichiers de conf
GENERATE.each do |v|
  create_conf v[0], v[1], env
end

